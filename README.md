<p align="center">
    <img src="https://media.giphy.com/media/3o7aCTfyhYawdOXcFW/giphy.gif" width="30%">
</p>

```Python
class Am:
    USERNAME = 'Paulo-Lopes-Estevao'
    NAME = 'Paulo Lopes Estevão'
    EMAIL = 'pl1745240@gmail.com'
    LINKEDIN = '[/in/paulolopesestevão](https://www.linkedin.com/in/paulo-lopes-estev%C3%A3o-7a70881b4/)'

    def Skills(self):
        CODE = dict(
            backend= ['Python', 'PHP', 'Golang','],
            frontend= ['HTML', 'CSS', 'JavaScript', 'Metro UI Css', 'Boostrap', 'Vuejs'],
            architecture= ['MVC', 'Clean Architeture', 'Architeture Horzagonal'],
            database= ['PostgreSQL', 'MySQL','SQlite],
            tools= ['VSCode', 'PyCharm', 'git', 'GitHub', 'heroku', 'postman'],
            so= ['Linux']
        )
        return CODE

    def DailyKnowledge(self):
        used = dict[
            ":Python",
            ":System architecture",
            ":TDD"
        ]

    def getActivity(self):
        self.NAME
        frequent= self.Skills()
        frequent['backend']

                                                                                      # Paulo Lopes Estevão


```


<h2 align="center">I love Coding 💻</h2>

---


<h4 align="center">Languages & Tools <i class="devicon-python-plain"></i></h4>
<p align="center">

<a href="https://golang.org/" target="_blank">
        <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/go/go-original.svg" alt="golang" width="40" height="40"/>
</a>
<a href="https://python.org/" target="_blank">
        <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/python/python-original.svg" alt="python" width="40" height="40"/>
</a>
<a href="https://php.org/" target="_blank">
        <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/php/php-original.svg" alt="php" width="40" height="40"/>
</a>
<a href="https://javascript.org/" target="_blank">
        <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/javascript/javascript-original.svg" alt="javascript" width="40" height="40"/>
</a>
<a href="https://docker.org/" target="_blank">
        <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/docker/docker-original.svg" alt="docker" width="40" height="40"/>
</a>
<a href="https://graphql.org/" target="_blank">
        <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/graphql/graphql-plain.svg" alt="graphql" width="40" height="40"/>
</a>
<img src="skilz/rest-api.svg" width="40px" height="40px"/>
<a href="https://laravel.org/" target="_blank">
        <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/laravel/laravel-plain.svg" alt="laravel" width="40" height="40"/>
</a>
<img src="skilz/file_type_kivy_icon_130489.svg" width="40px" height="40px"/>
<a href="https://git.org/" target="_blank">
        <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/git/git-original.svg" alt="git" width="40" height="40"/>
</a>
<a href="https://github.org/" target="_blank">
        <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/github/github-original.svg" alt="github" width="40" height="40"/>
</a>
<a href="https://postman.com" target="_blank">
        <img src="https://www.vectorlogo.zone/logos/getpostman/getpostman-icon.svg" alt="postman" width="40" height="40"/>
</a>
    <br/>
    <br/>
<a href="https://vue.org/" target="_blank">
        <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/vuejs/vuejs-original.svg" alt="vuejs" width="40" height="40"/>
</a>
<a href="https://code.visualstudio.com/" target="_blank">
        <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/vscode/vscode-original.svg" alt="vscode" width="40" height="40"/>
    </a>
<a href="https://www.postgresql.org" target="_blank">
        <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/postgresql/postgresql-original-wordmark.svg" alt="postgresql" width="40" height="40"/>
    </a>
  <a href="https://www.mysql.com/" target="_blank">
        <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/mysql/mysql-original-wordmark.svg" alt="mysql" width="40" height="40"/>
    </a>
<img src="skilz/sqlite-icon.svg" width="40px" height="40px"/>
<a href="https://ubuntu.org/" target="_blank">
        <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/ubuntu/ubuntu-plain.svg" alt="ubuntu" width="40" height="40"/>
</a>
<a href="https://www.linux.org/" target="_blank">
        <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/linux/linux-original.svg" alt="linux" width="40" height="40"/>
    </a>
<a href="https://kubernetes.org/" target="_blank">
        <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/kubernetes/kubernetes-plain.svg" alt="kubernetes" width="40" height="40"/>
</a>
<img src="skilz/argo-horizontal-color.svg" width="80px" height="40px"/>
<a href="https://jenkins.org/" target="_blank">
        <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/jenkins/jenkins-plain.svg" alt="jenkins" width="40" height="40"/>
</a>
<img src="skilz/Terraform_Logo.svg.png" width="120px" height="40px"/>
</p>



<h4 align="center">My Profiles</h4>
<p align="center">

  <a href="https://www.facebook.com/paulodoposter.poster.1">
    <img alt="Paulo Lopes Estevão" src="https://img.shields.io/badge/-facebook-blue?style=flat-circle&logo=Facebook&logoColor=white&link=https://www.facebook.com/paulodoposter.poster.1">
  </a>

  <a href="https://www.linkedin.com/in/paulo-lopes-estev%C3%A3o-7a70881b4/">
    <img alt="Paulo Lopes Estevão" src="https://img.shields.io/badge/-LinkedIn-blue?style=flat-circle&logo=Linkedin&logoColor=white&link=https://www.linkedin.com/in/paulo-lopes-estev%C3%A3o-7a70881b4/">
  </a>
  
  <a href="mailto:pl1745240@gmail.com">
    <img alt="Paulo Lopes Estevão" src="https://img.shields.io/badge/-Gmail-c14438?style=flat-circle&logo=Gmail&logoColor=white&link=mailto:pl1745240@gmail.com">
  </a>
  

</p>

<div>

[![Top Langs](https://github-readme-stats.vercel.app/api/top-langs/?username=Paulo-Lopes-Estevao&layout=compact)](https://github.com/Paulo-Lopes-Estevao/github-readme-stats)


<div style="width:25rem;">


[![willianrod's wakatime stats](https://github-readme-stats.vercel.app/api/?username=Paulo-Lopes-Estevao&count_private=true&bg_color=30,e96443,904e95&title_color=fff&text_color=fff)](https://github.com/Paulo-Lopes-Estevao/github-readme-stats)


</div>

</div>


### To contribute to open source 🙂
### Teamwork 💻
![](https://visitor-badge.glitch.me/badge?page_id=Paulo-Lopes-Estevao.Paulo-Lopes-Estevao)
